package com.epam.view;

import com.epam.model.Project;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {


    private static Logger logger = LogManager.getLogger(View.class);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private Project project = new Project();

    public View() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("1", "  1 - Add task to product backlog.");
        menu.put("2", "  2 - Add task to sprint backlog.");
        menu.put("3", "  3 - Add task in progress.");
        menu.put("4", "  4 - Add task to peer review.");
        menu.put("5", "  5 - Add task in test.");
        menu.put("6", "  6 - Add task to done.");
        menu.put("7", "  7 - Add task to blocked.");
        menu.put("8", "  8 - Get task list.");
        menu.put("Q", "  Q - exit");

        methodsMenu.put("1", this::addToProductBacklog);
        methodsMenu.put("2", this::addToSprintBacklog);
        methodsMenu.put("3", this::addToProgress);
        methodsMenu.put("4", this::addToReview);
        methodsMenu.put("5", this::addToInTest);
        methodsMenu.put("6", this::addToDone);
        methodsMenu.put("7", this::addToBlocked);
        methodsMenu.put("8", this::getTasksList);
        methodsMenu.put("Q", this::exit);
    }

    private void outputMenu() {
        logger.info("Menu:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    private void addToProductBacklog() {
        project.addToProductBacklog();
    }

    private void addToSprintBacklog() {
        project.addToSprintBacklog();
    }

    private void addToProgress() {
        project.addToProgress();
    }

    private void addToReview() {
        project.addToReview();
    }

    private void addToInTest() {
        project.addToInTest();
    }

    private void addToDone() {
        project.addToDone();
    }

    private void addToBlocked() {
        project.addToBlocked();
    }

    private void getTasksList() {
        project.getTasksList();
    }

    private void exit() {
        System.exit(0);
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point:");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (IOException e) {
                logger.error("IO error.");
            }
        } while (!keyMenu.equals("Q"));
    }
}