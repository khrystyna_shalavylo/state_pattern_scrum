package com.epam.controller;

import com.epam.model.Project;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProductBacklog implements State {

    private Logger logger = LogManager.getLogger(ProductBacklog.class);

    @Override
    public void addToProductBacklog(Project project) {
        project.addTaskToProjectList();
    }

    @Override
    public void addToSprintBacklog(Project project) {
        project.setState(new Sprint());
        int lastTask = project.tasks.size() - 1;
        logger.info(String.format("%s in sprint backlog", project.tasks.get(lastTask).toString()));
    }
}
