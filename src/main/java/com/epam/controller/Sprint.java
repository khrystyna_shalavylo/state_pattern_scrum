package com.epam.controller;

import com.epam.model.Project;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Sprint implements State {

    private Logger logger = LogManager.getLogger(Sprint.class);

    @Override
    public void addToProgress(Project project){
        project.setState(new InProgress());
        int lastTask = project.tasks.size() - 1;
        logger.info(String.format("%s in progress", project.tasks.get(lastTask).toString()));
    }
}
