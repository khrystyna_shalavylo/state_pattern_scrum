package com.epam.controller;

import com.epam.model.Project;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PeerReview implements State {

    private Logger logger = LogManager.getLogger(PeerReview.class);

    @Override
    public void addToInTest(Project project){
        project.setState(new InTest());
        int lastTask = project.tasks.size() - 1;
        logger.info(String.format("%s is tested", project.tasks.get(lastTask).toString()));
    }

    @Override
    public void addToProgress(Project project){
        project.setState(new InProgress());
        int lastTask = project.tasks.size() - 1;
        logger.info(String.format("%s in progress", project.tasks.get(lastTask).toString()));
    }

    @Override
    public void addToBlocked(Project project) {
        project.setState(new Blocked());
        int lastTask = project.tasks.size() - 1;
        logger.info(String.format("%s is blocked", project.tasks.get(lastTask).toString()));
        logger.info("Add other tasks");
    }
}
