package com.epam.controller;

import com.epam.model.Project;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InProgress implements State {
    private Logger logger = LogManager.getLogger(InProgress.class);

    @Override
    public void addToReview(Project project) {
        project.setState(new PeerReview());
        int lastTask = project.tasks.size() - 1;
        logger.info(String.format("%s is reviewed", project.tasks.get(lastTask).toString()));
    }
}
