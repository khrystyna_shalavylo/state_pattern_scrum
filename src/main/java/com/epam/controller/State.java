package com.epam.controller;

import com.epam.model.Project;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {

    Logger logger = LogManager.getLogger(State.class);

    default void addToProductBacklog(Project project) {
        logger.info("Add to product backlog is not allowed");
    }

    default void addToSprintBacklog(Project project) {
        logger.info("Add to sprint backlog is not allowed");
    }

    default void addToProgress(Project project) {
        logger.info("Add to progress is not allowed");
    }

    default void addToReview(Project project) {
        logger.info("Add to peer review is not allowed");
    }

    default void addToInTest(Project project) {
        logger.info("Add in test is not allowed");
    }

    default void addToDone(Project project) {
        logger.info("Add to done is not allowed");
    }

    default void addToBlocked(Project project) {
        logger.info("Add to blocked is not allowed");
    }
}
