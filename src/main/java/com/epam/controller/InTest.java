package com.epam.controller;

import com.epam.model.Project;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InTest implements State {

    private Logger logger = LogManager.getLogger(InTest.class);

    @Override
    public void addToProgress(Project project) {
        project.setState(new InProgress());
        int lastTask = project.tasks.size() - 1;
        logger.info(String.format("%s in progress", project.tasks.get(lastTask).toString()));
    }

    @Override
    public void addToDone(Project project) {
        project.setState(new Done());
        int lastTask = project.tasks.size() - 1;
        logger.info(String.format("%s is done", project.tasks.get(lastTask).toString()));
        logger.info("Add other tasks");
    }
}
