package com.epam.model;

public class Task {
    private static int count;
    private int taskNumber;

    public Task() {
        count++;
        taskNumber = count;
    }

    @Override
    public String toString() {
        return "Task_" + taskNumber;
    }
}
