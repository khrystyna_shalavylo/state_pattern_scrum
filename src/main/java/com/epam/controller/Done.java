package com.epam.controller;

import com.epam.model.Project;

public class Done implements State {
    @Override
    public void addToProductBacklog(Project project) {
        project.addTaskToProjectList();
        project.setState(new ProductBacklog());
    }
}
