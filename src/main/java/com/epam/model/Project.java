package com.epam.model;

import com.epam.controller.ProductBacklog;
import com.epam.controller.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Project {
    private State state;
    public List<Task> tasks = new ArrayList<>();
    private Logger logger = LogManager.getLogger(Project.class);

    public Project() {
        state = new ProductBacklog();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void addToProductBacklog() {
        state.addToProductBacklog(this);
    }

    public void addToSprintBacklog() {
        if (tasks.isEmpty()) {
            logger.info("List of tasks is empty");
        } else {
            state.addToSprintBacklog(this);
        }
    }

    public void addToProgress() {
        state.addToProgress(this);
    }

    public void addToReview() {
        state.addToReview(this);
    }

    public void addToInTest() {
        state.addToInTest(this);
    }

    public void addToDone() {
        state.addToDone(this);
    }

    public void addToBlocked() {
        state.addToBlocked(this);
    }

    public void addTaskToProjectList(){
        Task task=new Task();
        tasks.add(task);
        logger.info(task+" added to product backlog");
    }

    public void getTasksList(){
        System.out.println(tasks);
    }
}
