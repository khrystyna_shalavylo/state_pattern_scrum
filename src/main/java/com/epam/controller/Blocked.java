package com.epam.controller;

import com.epam.model.Project;

public class Blocked implements State {
    @Override
    public void addToProductBacklog(Project project) {
        int lastTask = project.tasks.size() - 1;
        project.tasks.remove(lastTask);
        project.addTaskToProjectList();
        project.setState(new ProductBacklog());
    }
}
